<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//use App\Http\Controllers\ReportController;

Auth::routes();

Route::group(['middleware' => ['web']], function () {

    Route::get('/', 'ReportController@user')->name('index');
    Route::get('/users', 'AdminController@users')->name('users')->middleware('can:read');
    Route::post('/users', 'AdminController@saveRoles')->name('user-roles')->middleware('can:read');
    Route::get('/role-abilities', 'AdminController@roleAbilities')->name('role-abilities')->middleware('can:read');
    //Route::post('/save-role-abilities', 'AdminController@roleAbilities')->name('save-role-abilities')->middleware('can:read');
});

Route::group([
  'prefix' => 'reports',
  'middleware' => 'can:read'
], function () {

	Route::get('/', 'ReportController@report')->name('report');
	Route::get('/all', 'ReportController@reports')->name('reports');
	Route::get('/new', 'ReportController@new')->name('report-new');
	Route::post('/store', 'ReportController@store')->name('report-store');
	Route::get('/my-reports', 'ReportController@user')->name('user-reports');
	Route::get('/edit/{id}', 'ReportController@edit')->name('report-edit');
	Route::get('/delete/{id}', 'ReportController@delete')->name('report-edit');
	Route::get('/display/{name}', 'ReportController@display')->name('report-display');
	Route::get('/download/{name}', 'ReportController@download')->name('report-download');

	Route::get('/sale-consult', 'ReportController@getSales')->name('sale-consult');
	Route::post('/fetch-sales', 'ReportController@fetchSales')->name('fetch-sales');
	Route::get('/sale-form', 'ReportController@fetchSaleForm')->name('sale-form');
	Route::get('/new-sale/{id}', 'ReportController@newSale')->name('new-sale-download');

	Route::get('/config', 'AdminController@config')->name('config');
	Route::post('/config-process', 'AdminController@processConfig')->name('config-process');

	/*  Getters  */
	Route::get('/download', 'ReportController@downloads')->name('report-downloads');
	Route::get('/edit', 'ReportController@display')->name('report-edits');
	Route::get('/new-sale', 'ReportController@newSale')->name('new-sale');
});

Route::get('login', ['as' => 'login', 'uses' => 'OauthController@redirectToProvider']);

Route::get('login_callback', ['as' => 'login_callback', 'uses' => 'OauthController@handleProviderCallback']);

Route::get('/risk', 'ReportController@risk')->name('risk-data');
Route::get('/test', 'ReportController@processQueue');
Route::get('/threads', 'ThreadController@fetchPending');

Route::post('/config-partner', 'AdminController@savePartnerData')->name('save-config-partners');
Route::post('/config-endpoint', 'AdminController@saveEndpointData')->name('save-config-endpoints');




//! FALLBACK

Route::fallback(function () {
	return 'Hm, why did you land here somehow?';
	// return (new ReportController)->report();
});
