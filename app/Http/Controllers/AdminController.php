<?php

namespace App\Http\Controllers;

use App\Entities\User;
use App\Managers\AdminManager as Manager;
use Illuminate\Http\Request;
use Silber\Bouncer\BouncerFacade as Bouncer;
use Silber\Bouncer\Database\Role;

// use Illuminate\Http\Request;

class AdminController extends Controller
{
	protected $manager;
	protected $oauth;
	protected $user;

	public function __construct()
	{
		$this->user = new User;
		$this->manager = new Manager;
		$this->bouncer = new Bouncer;
	}

	public function index()
	{
		return view('layouts.app');
	}

	public function users()
	{
		$users = $this->user->get();

		$roles = [
			'1' => 'admin',
			'2' => 'plebe',
		];
		// dd(Role::all(), $roles);

		return view('user.all')->with(compact('users', 'roles'));
	}

	public function saveRoles(Request $request)
	{
		$user = $this->user->where('id', $request->uid)->first();

		Bouncer::sync($user)->roles([]);
		Bouncer::assign($request->role)->to($user);

		return $this->users();
	}

	public function roleAbilities(Request $request)
	{
		$roles = $this->manager->getRoles();
		$abilities = $this->manager->getAbilities();
		$users = $this->user->get();

		return view('bouncer.config')->with(compact('roles', 'abilities', 'users'));
	}

	public function config()
	{
		$endpoints = $this->manager->getConfig('endpoint');
		$partners = $this->manager->getConfig('partner');

		return view('config')->with(compact('endpoints', 'partners'));
	}

	public function savePartnerData(Request $request)
	{
		$partner_id = $request->partner_id;
		$value = $request->value;

		$this->manager->updatePartner($partner_id, $value);

		return $this->config();
	}

	public function saveEndpointData(Request $request)
	{
		$endpoint_id = $request->endpoint_id;
		$value = $request->endpoint;

		$this->manager->updateEndpoint($endpoint_id, $value);

		return $this->config();
	}
}
