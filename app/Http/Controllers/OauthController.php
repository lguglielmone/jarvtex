<?php

namespace App\Http\Controllers;

use App\Entities\User;
use App\Http\Controllers\Auth\LoginController;
use App\Repositories\OauthRepo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Socialite;

class OauthController extends Controller
{
	protected $repo;
	protected $auth;
	protected $provider = 'aperoauth';

	public function __construct()
	{
		$this->repo = new OauthRepo;
		$this->auth = new LoginController;
	}

	public function modal(Request $request)
	{
		return view('user.login');
	}

	public function redirectToProvider()
	{
		return Socialite::with($this->provider)->redirect();
	}

	public function handleProviderCallback()
	{
		$oauth_user = Socialite::with($this->provider)->user();

		if ($user = User::where('email', $oauth_user->email)->first()) {
			return $this->_authAndRedirect($user); // Login y redirección
		} else {
			// En caso de que no exista creamos un nuevo usuario con sus datos.
			$user = User::create([
				'name' => $oauth_user->name,
				'email' => $oauth_user->email,
				//'avatar' => $oauth_user->avatar,
			]);

			return $this->_authAndRedirect($user); // Login y redirección
		}
	}

		private function _authAndRedirect($user)
		{
			Auth::login($user);

			return redirect()->to('/');
		}

}
