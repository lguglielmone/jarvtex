<?php

namespace App\Http\Controllers;

use App\Managers\ThreadManager;
use App\Managers\FileManager;
use Illuminate\Support\Facades\Log;

class ThreadController extends Controller
{
	protected $manager;
	protected $file;
	protected $validator;

	public function __construct()
	{
		$this->manager = new ThreadManager;
		$this->file = new FileManager;
		$this->log = new Log;
	}

	public function fetchPending()
	{
		$pending_threads = $this->manager->fetchPending();
// dd($pending_threads);
		if ($pending_threads) {
			Log::info('ReportController - pendingThreads - START - Discovered '.count($pending_threads).' Reports');
			$this->manager->process($pending_threads);
			Log::info('ReportController - pendingThreads - FINISH');
		}
	}
}
