<?php

namespace App\Http\Controllers;

use App\Entities\Report;
use App\Managers\AdminManager;
use App\Managers\ReportManager;
use App\Managers\FileManager;
use App\Validators\ReportValidator;
use App\Validators\FileValidator;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ReportController extends Controller
{
	protected $admin;
	protected $file;
	protected $manager;
	protected $validator;

	public function __construct()
	{
		$this->manager = new ReportManager;
		$this->validator = new ReportValidator;
		$this->admin = new AdminManager;
		$this->file = new FileManager;
		$this->log = new Log;
		$this->client = new Client;
	}

	public function reports()
	{
		$reports = $this->manager->all();

		return view('reports.all')->with('reports', $reports);
	}

	public function report()
	{
		//
	}

	public function new()
	{
		$id = $this->manager->fetchId();

		return view('reports.new')->with('id', $id);
	}

	public function store(Request $request)
	{
		if ($this->validator->isValid($request)) {

			return $this->manager->store($request);
		}

		return view('reports.new')->with('notification', 'error');
	}

	public function edit($id)
	{
		$this->manager->delete($id);

		return $this->reports();
	}

	public function delete(Report $id)
	{
		$this->manager->delete($id);

		return $this->user();
	}

	public function user()
	{
		return $this->manager->fetchForUser();
	}

	public function processQueue()
	{
		$pending_reports = $this->manager->fetchPending();
		// dd($pending_reports);
		if ($pending_reports) {
			Log::info('ReportController - processQueue - START - Discovered '.count($pending_reports).' Reports');
			$this->manager->process($pending_reports);
			Log::info('ReportController - processQueue - FINISH');
		}
	}

	public function display($report_name)
	{
		// return $this->file->display($report_name);
	}

	public function download($name)
	{
		return $this->file->export($name);
	}

	public function risk()
	{
		$risk_data = json_decode($this->_connect('Api-Risk'));
		// dd($risk_data);
		return view('reports.risk')->with('reports', $risk_data);
	}

	private function _connect($name)
	{
		if ($name == 'Api-Risk') {
			$risk_api_url = 'http://localhost:5000/report';

			$response = $this->client->request('GET', $risk_api_url);

			return $response->getBody()->getContents();

		} else if ($name == 'Other Stuff') {
			// do something
		}
	}

	public function getSales()
	{
		$files = $this->manager->getSales();
		// $partners = $this->admin->getConfig('partner');

		return view('reports.sales')->with(compact('files'));
	}

	public function fetchSales(Request $request)
	{
		if ($this->validator->fetchSales($request)) {
			$this->manager->fetchSales($request);
		}

		return $this->getSales();
	}

	public function fetchSaleForm()
	{
		// $partners = $this->admin->getConfig('partner');
		$partners = $this->manager->fetchPartners();

		return view('reports.sale')->with(compact('partners'));
	}
}
