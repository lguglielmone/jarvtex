<?php

namespace App\Entities\App;

use Illuminate\Database\Eloquent\Model;


class Configuration extends Model
{
	protected $fillable = [
		'name',
		'value',
		'slug',
	];

}
