<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
	protected $fillable = [
		'name',
		'state',
		'report_id',
		'blame',
		'partner'
	];

	public function report()
	{
		return $this->belongsTo('App\Entities\Report');
	}
}
