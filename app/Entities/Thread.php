<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Thread extends Model
{
	protected $fillable = [
		'customer',
		'supplier',
		'thread_id',
		'order_id',
	];
}
