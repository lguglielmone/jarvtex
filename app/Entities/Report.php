<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
	protected $fillable = [
		'user_id',
		'name',
		'query',
		'type',
		'when',
		'description',
		'active'
	];

	public function user()
	{
		return $this->belongsTo('App\Entities\User');
	}

	public function file()
	{
		return $this->hasMany('App\Entities\File');
	}
}
