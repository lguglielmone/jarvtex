<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Managers\ReportManager;

class ProcessQueue extends Command
{
	/**
	* The name and signature of the console command.
	*
	* @var string
	*/
	protected $signature = 'process:queue';

	/**
	* The console command description.
	*
	* @var string
	*/
	protected $description = 'Runs pending reports';

	protected $manager;

	/**
	* Create a new command instance.
	*
	* @return void
	*/
	public function __construct()
	{
		parent::__construct();

		$this->manager = new ReportManager;
	}

	/**
	* Execute the console command.
	*
	* @return mixed
	*/
	public function handle()
	{
		if ($pending_threads = $this->manager->fetchPending()) {
			Log::info('ReportController - pendingThreads - START');
			Log::info('ReportController - pendingThreads - Discovered '.count($pending_threads).' Reports');
			$this->manager->process($pending_threads);
			Log::info('ReportController - pendingThreads - FINISH');
		}
	}
}
