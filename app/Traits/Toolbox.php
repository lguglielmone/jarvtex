<?php

namespace App\Traits;

trait Toolbox
{
	/**
	 * Expects $request values:
	 * @date-from-day
	 * @date-from-month
	 * @date-from-year
	 * @date-to-day
	 * @date-to-month
	 * @date-to-year
	*/
	public function fixDate($request)
	{
		$date = [];
		$day_from = $request['date-from-day'];
		$month_from = $request['date-from-month'];
		$year_from = $request['date-from-year'];
		$day_to = $request['date-to-day'];
		$month_to = $request['date-to-month'];
		$year_to = $request['date-to-year'];

		$from = date('Y-m-d', strtotime("$day_from.$month_from.$year_from"));
		$to = date('Y-m-d H:i:s', strtotime('+23 Hours 59 Minutes 59 Seconds', strtotime("$day_to.$month_to.$year_to")));

		$date['from'] = $from;
		$date['to'] = $to;

		return $date;
	}

	public function formatPesos($pesos)
	{
		$decimals = substr($pesos, -2);
		$integers = substr($pesos, 0, -2);

		$number = (float) ($integers . '.' . $decimals);

		$pesos = number_format($number, 2, '.', '');

		return $pesos;
	}
}
