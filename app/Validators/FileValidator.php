<?php

namespace App\Validators;

use \stdClass;
use App\Entities\File;

/**
* SessionToken validaty checker.
*/
class FileValidator
{

  public $response;
  protected $file;

  public function __construct()
  {
    $this->file = new File;
    $this->response = new stdClass;
    $this->_init();
  }

  public function exists(int $partner_id, string $transaction_id)
  {
    $start = microtime(true);

    try {

      $conditions = compact('transaction_id', 'partner_id');

      $this->sale = $this->sale->where($conditions)->first();

      if (is_null($this->sale)) {
        $this->response->body = ["description" => "Sale not found (please verify transaction_id)."];
        $this->response->statusCode = 400;
        $this->response->time = (int)((microtime(true) - $start) * 1000);
        return false;
      }
      if ($this->sale->canceled == 1) {
        $this->response->body = ["description" => "Sale already reverted."];
        $this->response->statusCode = 400;
        $this->response->time = (int)((microtime(true) - $start) * 1000);
        return false;
      }

      return true;

    } catch (Exception $e) {
      Log::error($e);
    }

    return false;
  }

  public function cancel($request)
  {
    $start = microtime(true);

    if (!$request->filled('transaction_id')) {
      return $this->_failResponse(400, $start, "Please provide transaction_id.");
    }
    if (!$request->filled('user_purchase_selection') && !$request->filled('user_rewards_to_refund')) {
      return $this->_failResponse(400, $start, "Please provide user_purchase_selection or user_rewards_to_refund.");
    }

    if ($request->filled('user_purchase_selection') && !$request->filled('user_rewards_to_refund')) {

      if (!$request->filled('user_purchase_selection.amount')) {
        return $this->_failResponse(400, $start, "Please provide an amount.");
      }
      if (!is_numeric($request->user_purchase_selection['amount'])) {
        return $this->_failResponse(400, $start, "Please provide a valid amount.");
      }
      if (!$request->filled('user_purchase_selection.currency_code')) {
        return $this->_failResponse(400, $start, "Please provide a currency_code.");
      }
      if (!$request->filled('additional_info')) {
        return $this->_failResponse(400, $start, "Please provide additional_info.");
      }
      if (!$request->filled('additional_info.reason')) {
        return $this->_failResponse(400, $start, "Please provide a reason.");
      }
    }

    if ($request->filled('user_rewards_to_refund') && !$request->filled('user_purchase_selection')) {

      if (!$request->filled('user_rewards_to_refund.amount')) {
        return $this->_failResponse(400, $start, "Please provide an amount.");
      }
      if (!is_numeric($request->user_rewards_to_refund['amount'])) {
        return $this->_failResponse(400, $start, "Please provide a valid amount.");
      }
      if (!$request->filled('user_rewards_to_refund.currency_code')) {
        return $this->_failResponse(400, $start, "Please provide a currency_code.");
      }
      if (!$request->filled('user_rewards_to_refund.rewards')) {
        return $this->_failResponse(400, $start, "Please provide rewards.");
      }
      if (!is_numeric($request->user_rewards_to_refund['rewards'])) {
        return $this->_failResponse(400, $start, "Please provide a valid input for rewards.");
      }
      if (!$request->filled('user_rewards_to_refund.conversion_rate')) {
        return $this->_failResponse(400, $start, "Please provide a conversion_rate.");
      }
      if (!is_numeric($request->user_rewards_to_refund['conversion_rate'])) {
        return $this->_failResponse(400, $start, "Please provide a valid conversion_rate.");
      }
    }

    return true;
  }

  public function add($request)
  {
    $start = microtime(true);

    if (!$request->filled('user_purchase_selection')) {
      return $this->_failResponse(400, $start, "Please provide user_purchase_selection.");
    }
    if (!$request->filled('user_purchase_selection.currency_code')) {
      return $this->_failResponse(400, $start, "Please provide currency_code.");
    }
    if (!$request->filled('user_purchase_selection.amount')) {
      return $this->_failResponse(400, $start, "Please provide amount.");
    }
    if (!is_numeric($request->user_purchase_selection['amount'])) {
      return $this->_failResponse(400, $start, "Please provide a valid amount.");
    }
    if (!$request->filled('product_type')) {
      return $this->_failResponse(400, $start, "Please provide product_type.");
    }
    if (!$request->filled('product_description')) {
      return $this->_failResponse(400, $start, "Please provide product_description.");
    }
    if (!$request->filled('location_type')) {
      return $this->_failResponse(400, $start, "Please provide location_type.");
    }

    return true;
  }

  private function _init()
  {
    $this->response->body = Array(
      'description' => 'Something went wrong. Please try again later.',
      'code' => 501
    );

    $this->response->statusCode = 500;
    $this->response->time = 0;
  }

  private function _failResponse($statusCode, $time, $description)
  {
    $this->response->body = ["description" => $description];
    $this->response->statusCode = $statusCode;
    $this->response->time = (int)((microtime(true) - $time) * 1000);

    return false;
  }
}
