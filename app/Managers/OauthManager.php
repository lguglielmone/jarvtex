<?php

namespace App\Managers;

use App\Repositories\UserRepo;
use App\Entities\User;
//use App\Validators\OauthValidator;
//use \stdClass;

class OauthManager
{
	protected $repo;
	protected $validator;
	protected $user;

	public function __construct()
	{
		$this->repo = new UserRepo;
		//$this->validator = new ReportValidator;
		$this->user = new User;
	}

	public function login()
	{
		$session_token = $this->_getToken();

		return view('user.login')->with('session_token', $session_token);
	}

	public function all()
	{
		// $reports = $this->user->all();

		// return view('reports.all', compact('reports'));
	}

	public function store($data)
	{
		// $report = $this->repo->store($data);

		// return view('reports.new', ['notification' => 'success']);
	}

	private function _getToken()
	{
		return env('OAUTH_HASH');
	}
}
