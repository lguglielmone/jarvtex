<?php

namespace App\Managers;

use App\Entities\File as Csv;
use App\Managers\AppManager;
use Illuminate\Support\Facades\Storage;
// use Illuminate\Support\Facades\Redirect;
// use Illuminate\Support\Facades\Session;

//use App\Validators\FileValidator;

class FileManager extends AppManager
{
	protected $file;

	public function __construct()
	{
		$this->file = new Csv;

		parent::__construct();
	}

	public function fetchForUser()
	{
		$user = $this->fetchUser();

		$files = $this->repo->fetchForUser($user->id);

		return view('reports.display')->with(compact('user', 'reports'));
	}

	public function display($name)
	{
		// $path = "localhost:8000/storage/app/$name.csv";

		// return response()->file($path);
	}

	public function export($name)
	{
		if (Storage::disk('report')->exists("$name.csv")) {
				return Storage::disk('report')->download("$name.csv");
		}
	}

	private function _fetchExport($name)
	{
		// 		$file = File::get($name) ?? false;
		// 		dd($file);
		// 		return $file;
	}
}
