<?php

namespace App\Managers;

use App\Entities\User;
use App\Traits\Toolbox;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use \stdClass;

class AppManager
{
	use Toolbox;

	protected $auth;
	protected $log;

	public function __construct()
	{
		$this->log = new Log;
	}

	public function fetchUser()
	{
		return Auth::user();
	}

	public function fetchId()
	{
		return Auth::user()->id;
	}
}
