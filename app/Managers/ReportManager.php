<?php

namespace App\Managers;

use App\Entities\File;
use App\Entities\Report;
use App\Managers\AppManager;
use App\Managers\AdminManager;
use App\Repositories\ReportRepo;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

//use App\Validators\ReportValidator;

class ReportManager extends AppManager
{
  protected $repo;
  protected $validator;
  protected $model;
  protected $file;
  protected $admin;

  public function __construct()
  {
    $this->repo = new ReportRepo;
    //$this->validator = new ReportValidator;
    $this->model = new Report;
    $this->file = new File;
    $this->client = new Client;
    $this->admin = new AdminManager;

    parent::__construct();
  }

  public function all()
  {
    $reports = $this->model->all();//->orderBy('created_at', 'desc');

    return $reports;
  }

  public function fetch(int $id)
  {
    return $this->model->find($id);
  }

  public function store($data)
  {
    $data = $this->_reconstruct($data);

    $report = $this->repo->store($data) ?? false;

    $id = $this->fetchId();
    $notification = $report ? 'success' : 'fail';

    $conditions = compact('notification', 'id');

    return view('reports.new', $conditions);
  }

  public function delete(Report $report)
  {
    if ($report->delete()) {
      return true;
    }

    return false;
  }

  public function fetchPending()
  {
    $conditions = $this->queueConditions();

    return $this->repo->fetchPending($conditions);
  }

  public function fetchPendingThreads()
  {
    return $this->repo->fetchPendingThreads();
  }

  public function process($reports)
  {
    // Run Query
    foreach ($reports as $report) {
      // Handle errors

      // Stored Report in /storage/reports
      $file = $this->repo->createAndSave($report);

      // Create Link
      $this->_notifyClient($file);
    }
  }

  public function fetchForUser()
  {
    if (!$user = $this->fetchUser()) {
      return view('layouts.app');
    }

    $reports = $this->repo->fetchForUser($user->id);

    $pending = 0;

    foreach ($reports as $report) {
      if ($report->active == 0) {
        $pending++;
      }
    }

    return view('reports.display')->with(compact('user', 'reports', 'pending'));
  }

  public function getSales()
  {
    return $this->repo->getSales();
  }

  public function fetchSales($request)
  {
		$user_id = $request['user'];
		$partner = explode('|', $request['partner']);
    //$partner_slug = $request['partner'];
    $range = $this->fixDate($request);
    $options['form_params'] = $range;

    $url = $this->admin->getEndpoint('Servicio Intermedio');
    $prefix = '/api/sales/reports/';

    $endpoint = $url.$prefix.$partner[0];

    $result = $this->client->post($endpoint, $options);

    $sales = json_decode($result->getBody()->getContents());

    $data = $this->saveAsCsv($sales);

    $this->repo->newSale($data, $user_id, $partner[1]);

    $this->saveAsCsv($sales);
  }

  public function saveAsCsv($sales)
  {
    $text = $this->csvColumns();

    foreach ($sales->conversions->original as $conversion) {
      $conversions[$conversion->category_id] = $conversion->factor;
    }

    foreach ($sales->sales as $sale) {

      if (property_exists(json_decode($sale->info), 'referenceCode')) {

        $info = json_decode($sale->info);
        $product = $info->products[0];
        $referenceCode = $info->referenceCode;
        $totalPoints = $info->totalPoints;
        $totalPesos = $this->formatPesos($info->totalPesos);
        $totalPurchase = $this->formatPesos($info->totalPurchase);
        $retailerCUIT = $info->retailerCUIT;
        $retailerName = $info->retailerName;
        $productCode = '"'.$product->productCode.'"';
        $productName = '"'.$product->productName.'"';
				$productCategory = $product->productCategory;
        $productFactor = $conversions[$product->productCategory] ? '"'.$conversions[$product->productCategory].'"' : null;
        $productQuantity = $product->productQuantity;
				$productType = isset($info->productType) ? $info->productType : null;
				$locationType = isset($info->locationType) ? $info->locationType : null;

        $text .= $sale->transaction_id . ';';
        $text .= $sale->partner_id . ';';
        $text .= $sale->amount . ';';
        $text .= $referenceCode . ';';
        $text .= $totalPoints . ';';
        $text .= $totalPesos . ';';
        $text .= $totalPurchase . ';';
        $text .= $retailerCUIT . ';';
        $text .= $retailerName . ';';
        $text .= $sale->canceled . ';';
        $text .= $productCode . ';';
        $text .= $productName . ';';
        $text .= $productFactor . ';';
        $text .= $productCategory . ';';
        $text .= $productQuantity . ';';
        $text .= $productType . ';';
        $text .= $locationType . ';';
        $text .= $sale->created_at . "\n";
      }
    }

    return $text;
  }

  public function queueConditions()
  {
    $now = Carbon::now()->format('h:i');

    return $this->_cronize($now);
  }

  private function _reconstruct($data)
  {
    if ($data['report-interval-repeat'] == 'on') {
      $interval = 1;
    } else {
      $interval = 0;
    }

    $day = $data['report-day'];

    $hour = substr($data['report-hour'], 0, 2);
    $minute = substr($data['report-hour'], -2, 2);

    $crontab = "$minute $hour * * $day";

    $data = [
      'user_id' => $data['report-user'],
      'name' => $data['report-name'],
      'query' => $data['report-query'],
      'interval' => $interval,
      'crontab' => $crontab,
      'description' => $data['report-desc']
    ];

    return $data;
  }

  private function _cronize($date)
  {
    $day = Carbon::now()->dayOfWeek;
    $hour = substr($date, -5, 2);
    $minute = substr($date, -2, 2);

    $crontab = "$minute $hour * * $day";

    return $crontab;
  }

  private function _notifyClient($file)
  {
    // TODO
  }

  private function csvColumns()
  {
    $columns = '';

    $columns .= 'Transaction ID' . ';';
    $columns .= 'Partner ID' . ';';
    $columns .= 'Monto' . ';';
    $columns .= 'Referencia' . ';';
    $columns .= 'Puntos' . ';';
    $columns .= 'Pesos' . ';';
    $columns .= 'Puntos en Pesos' . ';';
    $columns .= 'Cuit' . ';';
    $columns .= 'Proveedor' . ';';
    $columns .= 'Cancelado' . ';';
    $columns .= 'ProductCode' . ';';
    $columns .= 'ProductName' . ';';
    $columns .= 'ProductFactor' . ';';
    $columns .= 'ProductCategory' . ';';
    $columns .= 'ProductQuantity' . ';';
    $columns .= 'ProductType' . ';';
    $columns .= 'LocationType' . ';';
    $columns .= 'Fecha' . "\n";

    return $columns;
	}

	public function fetchPartners()
	{
		$data = [];

		$url = $this->admin->getEndpoint('Servicio Intermedio');
		$endpoint = $url . '/api/config/partner';

		$result = $this->client->get($endpoint);

		$partners = json_decode($result->getBody()->getContents());

		foreach ($partners as $partner) {
			if ($partner->name != 'Icbc Club') {
				$data[] = [
					'name' => $partner->name,
					'slug' => $partner->slug
				];
			}
		}

		return $data;
	}
}
