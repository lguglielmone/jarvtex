<?php

namespace App\Managers;

use Silber\Bouncer\Database\Role;
use Silber\Bouncer\Database\Ability;
use App\Entities\App\Configuration;

class AdminManager extends AppManager
{
	protected $role;
	protected $ability;
	protected $config;

	public function __construct()
	{
		$this->role = new Role;
		$this->ability = new Ability;
		$this->config = new Configuration;

		parent::__construct();
	}

	public function getRoles()
	{
		return $this->role->all();
	}

	public function getAbilities()
	{
		return $this->ability->all();
	}

	public function newRole($data)
	{
		return true;
	}

	public function newAbility($data)
	{
		return true;
	}

	public function getConfig(string $value)
	{
		$data = $this->config->where('name', 'like', '%'.$value.'%')->get();

		return $data;
	}

	public function getEndpoint(string $value)
	{
		$data = $this->config->where('slug',$value)->first();

		return $data['value'];
	}

	public function updatePartner(int $id, string $value)
	{
		$this->config->find($id)->update([
			'value' => $value
		]);
	}

	public function updateEndpoint(int $id, string $value)
	{
		$this->config->find($id)->update([
			'value' => $value
		]);
	}

}
