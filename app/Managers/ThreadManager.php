<?php

namespace App\Managers;

use App\Entities\File;
use App\Entities\Thread;
use App\Managers\AppManager;
use App\Repositories\ThreadRepo;
use Carbon\Carbon;
use Faker\Provider\Uuid;
use Illuminate\Support\Facades\Log;

//use App\Validators\ReportValidator;

class ThreadManager extends AppManager
{
	protected $repo;
	protected $validator;
	protected $model;
	protected $file;

	public function __construct()
	{
		$this->repo = new ThreadRepo;
		//$this->validator = new ReportValidator;
		$this->model = new Thread;
		$this->file = new File;

		parent::__construct();
	}

	public function all()
	{
		$threads = $this->model->all();//->orderBy('created_at', 'desc');

		return $threads;
	}

	public function fetch(int $id)
	{
		return $this->model->find($id);
	}

	public function fetchPending()
	{
		return $this->repo->fetchPending();
	}

	public function process($threads)
	{
		// Storage::disk('report')->put($this->file->name.'.csv', $content);
		// $fp = fopen($csvFileName, 'w');

		// Save data on lcoal database
		foreach ($threads as $thread) {
			if (!$this->repo->exists($thread->id_customer_thread)) {
				$this->repo->store($thread);
			}
		}

		$this->_alocate();
		$csvFileName = Uuid::uuid().'.csv';

		$fp = fopen($csvFileName, 'w');

		foreach($threads as $thread){
				fputcsv($fp, (array)$thread);
		}

		fclose($fp);

		return $this->model;
	}

	private function _alocate()
	{
		$name = Uuid::uuid();
	}
}
