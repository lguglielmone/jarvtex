<?php
namespace App\Providers;

// use Illuminate\Http\Request;
use Laravel\Socialite\Two\AbstractProvider;
use Laravel\Socialite\Two\ProviderInterface;
use Laravel\Socialite\Two\User;

class AperOauthProvider extends AbstractProvider implements ProviderInterface
{
	protected $stateless = true;

	protected $url;

	public function __construct($clientId, $clientSecret, $redirectUrl, $guzzle = [])
	{
		parent::__construct($clientId, $clientSecret, $redirectUrl, $guzzle);
		$this->url = config('services.aperoauth.url');
	}
	/**
	* {@inheritdoc}
	*/
	protected function getAuthUrl($state)
	{
		return $this->buildAuthUrlFromBase($this->url . '/oauth/authorize', $state);
	}
	/**
	* {@inheritdoc}
	*/
	protected function getTokenUrl()
	{
		return $this->url . '/oauth/token';
	}
	/**
	* {@inheritdoc}
	*/
	public function getAccessToken($code)
	{
		$response = $this->getHttpClient()->post($this->getTokenUrl(), [
			'headers' => ['Authorization' => 'Basic ' . base64_encode($this->clientId . ':' . $this->clientSecret)],
			'body'    => $this->getTokenFields($code),
		]);

		return $this->parseAccessToken($response->getBody());
	}
	/**
	* {@inheritdoc}
	*/
	protected function getTokenFields($code)
	{
		return array_add(
			parent::getTokenFields($code), 'grant_type', 'authorization_code'
		);
	}
	/**
	* {@inheritdoc}
	*/
	protected function getUserByToken($token)
	{
		// $response = $this->getHttpClient()->get($this->url . '/oauth/clients', [
		$response = $this->getHttpClient()->get($this->url . '/api/v1/rewards/clientsData', [
			'headers' => [
				'Authorization' => 'Bearer ' . $token,
			],
		]);

		return json_decode($response->getBody(), true);
	}
		/**
		* {@inheritdoc}
		*/
		// protected function formatScopes(array $scopes)
		// {
		// 	return implode(' ', $scopes);
		// }
		/**
		* {@inheritdoc}
		*/
	protected function mapUserToObject($user)
	{
		return (new User)->setRaw($user)->map([
			'id'       => $user['id'] ?? null,
			'nickname' => $user['display_name']  ?? null,
			'name'     => $user['clientName']  ?? null,
			'email'     => $user['email']  ?? null,
			'avatar'   => !empty($user['images']) ? $user['images'][0]['url'] : null,
		]);
	}

	protected function getCodeFields($state = null)
	{
		$fields = [
			'client_id' => $this->clientId,
			'redirect_uri' => $this->redirectUrl,
			'scope' => $this->formatScopes($this->getScopes(), $this->scopeSeparator),
			'response_type' => 'code',
		];

		return array_merge($fields, $this->parameters);
	}

}
