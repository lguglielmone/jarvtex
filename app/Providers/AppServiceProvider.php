<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;

class AppServiceProvider extends ServiceProvider
{
	/**
	* Register any application services.
	*
	* @return void
	*/
	public function register()
	{
		//
	}

	/**
	* Bootstrap any application services.
	*
	* @return void
	*/
	public function boot()
	{
		$this->bootAperOauthSocialite();
	}

	private function bootAperOauthSocialite()
	{
		$socialite = $this->app->make('Laravel\Socialite\Contracts\Factory');
		$socialite->extend(
			'aperoauth',
			function ($app) use ($socialite) {
				$config = $app['config']['services.aperoauth'];
				return $socialite->buildProvider(AperOauthProvider::class, $config);
			}
		);
	}
}
