<?php

namespace App\Repositories;

use App\Entities\File;
use App\Entities\Thread;
use Faker\Provider\Uuid;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class ThreadRepo
{
	protected $thread;
	protected $file;

	public function __construct()
	{
		$this->thread = new Thread;
		$this->file = new File;
	}

	public function fetch()
	{
		//
	}

	public function exists($id)
	{
		if ($this->thread->find($id)) {
			return true;
		}

		return false;
	}

	public function store($data)
	{
		$this->thread->create([
			'customer' => $data->email,
			'supplier' => $data->supplier_name,
			'thread_id' => $data->id_customer_thread,
			'order_id' => $data->id_order,
		]);

		return $this->thread;
	}

	public function fetchPending()
	{
		// $limit = Carbon::now()->subMonths(6)->toDateTimeString();
		// $query = "SELECT id_customer_thread, id_order, email, supplier_name FROM ps_customer_thread WHERE status = 'open' AND date_add >= '$limit' ORDER BY id_customer_thread DESC;";
		$query = "SELECT id_customer_thread, id_order, email, supplier_name FROM ps_customer_thread WHERE status = 'open' ORDER BY id_customer_thread DESC;";
		$result = $this->_runQuery($query);
// dd($result);
		return $result;
	}

	private function _runQuery($query)
	{
		try {
			$result = DB::connection('mysql')->select($query);

			return $result;

		} catch (\Exception $e){
			Log::info('ReportRepo - process - Failed running Query ');
		}
	}

	private function _alocate($content)
	{
		if (Storage::disk('report')->put($this->file->name.'.csv', $content)) {
			$this->file->state = 0;

		} else {
			$this->file->state = 2;
		}
	}

	private function _reconstructQuery($query)
	{
		$columns = '';

		// Get Columns
		foreach ($query[0] as $key => $value) {
			$columns .= $key . ',';
		}
		$columns .= "\n";

		// Get Values
		foreach ($query as $q) {
			foreach ($q as $value) {
				$columns .= $value . ',';
			}
			$columns .= "\n";
		}

		// Remove leftover ','s

		return $columns;
	}
}
