<?php

namespace App\Repositories;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Log;

class ClientHttpBase
{
  protected $client;

  public function __construct()
  {
    $this->client = new Client;
  }

  protected function get(string $url, array $headers = [], int $timeout = 10)
  {
    $options = Array(
      'headers' => $headers,
      'http_errors' => false,
    );

    $time_start = microtime(true);

    try {

      // $this->_logger('******  GET  ****** - START', [
      //   'URL' => $url,
      //   'Options' => json_encode($options)
      // ]);

      $response = $this->client->request('GET', $url, $options);

      return $this->_response($response, $time_start, 'GET');

    } catch (ConnectException $e) {

      $response = (object)array(
        'statusCode' => 500,
        'reason' => 'Internal Server Error',
        'body' => (object)array(
          'errorCode' => 1001,
          'errorDescription' => $e->getMessage(),
          'errorMessage' => $e->getMessage()
        )
      );

      return $this->_responseExcetion($response, $time_start, 'GET');

    } catch (RequestException $e) {

      $response = (object)array(
        'statusCode' => 504,
        'reason' => 'Gateway Timeout',
        'body' => (object)array(
          'errorCode' => 1002,
          'errorDescription' => $e->getMessage(),
          'errorMessage' => $e->getMessage()
        )
      );

      return $this->_responseExcetion($response, $time_start, 'GET');
    }

  }

  protected function post(string $url, array $headers = [], array $body = [], int $timeout = 10)
  {
    $options = Array(
      'headers' => $headers,
      'json' => $body,
      'http_errors' => false,
    );

    $time_start = microtime(true);

    try {

      // $this->_logger('******  POST  ****** - START', [
      //   'URL' => $url,
      //   'Options' => json_encode($options)
      // ]);

      $response = $this->client->request('POST', $url, $options);

      return $this->_response($response, $time_start, 'POST');

    } catch (ConnectException $e) {

      $response = (object)array(
        'statusCode' => 500,
        'reason' => 'Internal Server Error',
        'body' => (object)array(
          'errorCode' => 1001,
          'errorDescription' => $e->getMessage(),
          'errorMessage' => $e->getMessage()
        )
      );

      return $this->_responseExcetion($response, $time_start, 'POST');

    } catch (RequestException $e) {

      $response = (object)array(
        'statusCode' => 504,
        'reason' => 'Gateway Timeout',
        'body' => (object)array(
          'errorCode' => 1002,
          'errorDescription' => $e->getMessage(),
          'errorMessage' => $e->getMessage()
        )
      );

      return $this->_responseExcetion($response, $time_start, 'POST');
    }

  }

  protected function put(string $url, array $headers = [], array $body = [], int $timeout = 10)
  {
    $options = Array(
      'headers' => $headers,
      'json' => $body,
      'http_errors' => false,
    );

    $time_start = microtime(true);

    try {

      // $this->_logger('******  PUT  ****** - START', [
      //   'URL' => $url,
      //   'Options' => json_encode($options)
      // ]);

      $response = $this->client->request('PUT', $url, $options);

      return $this->_response($response, $time_start, 'PUT');

    } catch (ConnectException $e) {

      $response = (object)array(
        'statusCode' => 500,
        'reason' => 'Internal Server Error',
        'body' => (object)array(
          'errorCode' => 1001,
          'errorDescription' => $e->getMessage(),
          'errorMessage' => $e->getMessage()
        )
      );

      return $this->_responseExcetion($response, $time_start, 'PUT');

    } catch (RequestException $e) {

      $response = (object)array(
        'statusCode' => 504,
        'reason' => 'Internal Server Error',
        'body' => (object)array(
          'errorCode' => 1002,
          'errorDescription' => $e->getMessage(),
          'errorMessage' => $e->getMessage()
        )
      );

      return $this->_responseExcetion($response, $time_start, 'PUT');
    }

  }

  protected function delete(string $url, array $headers = [], array $body = [], int $timeout = 10)
  {
    $options = Array(
      'headers' => $headers,
      'json' => $body,
      'http_errors' => false,
    );

    $time_start = microtime(true);

    try {

      // $this->_logger('******  DELETE  ****** - START', [
      //   'URL' => $url,
      //   'Options' => json_encode($options)
      // ]);

      $response = $this->client->request('DELETE', $url, $options);

      return $this->_response($response, $time_start, 'DELETE');

    } catch (ConnectException $e) {

      $response = (object)array(
        'statusCode' => 500,
        'reason' => 'Internal Server Error',
        'body' => (object)array(
          'errorCode' => 1001,
          'errorDescription' => $e->getMessage(),
          'errorMessage' => $e->getMessage()
        )
      );

      return $this->_responseExcetion($response, $time_start, 'DELETE');

    } catch (RequestException $e) {

      $response = (object)array(
        'statusCode' => 504,
        'reason' => 'Gateway Timeout',
        'body' => (object)array(
          'errorCode' => 1002,
          'errorDescription' => $e->getMessage(),
          'errorMessage' => $e->getMessage()
        )
      );

      return $this->_responseExcetion($response, $time_start, 'DELETE');
    }

  }

  private function _response($response, $time_start, $method)
  {
    $return = array(
      'statusCode' => $response->getStatusCode(),
      'reason' => $response->getReasonPhrase(),
      'body' => json_decode($response->getBody()->getContents()),
      'time' => (int)((microtime(true) - $time_start) * 1000)
    );

    // $this->_logger('******  ' . $method . '  ****** - END', [
    //   'Response' => json_encode($return)
    // ]);

    return (object)$return;
  }

  private function _responseExcetion($response, $time_start, $method)
  {
    $return = array(
      'statusCode' => $response->statusCode,
      'reason' => $response->reason,
      'body' => $response->body,
      'time' => (int)((microtime(true) - $time_start) * 1000)
    );

    // $this->_logger('******  ' . $method . '  ****** - END', [
    //   'Response' => json_encode($return)
    // ]);

    return (object)$return;
  }

  private function _logger(string $method, array $params)
  {
    $param_text = '';

    foreach ($params as $item => $value) {
      $param_text .= ' - ' . $item . ': ' . $value;
    }

    $text_log = get_class($this) . ' - ' . $method . ' ' . $param_text;

    \Log::debug($text_log);

  }
}
