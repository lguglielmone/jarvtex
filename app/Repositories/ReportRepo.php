<?php

namespace App\Repositories;

use App\Entities\File;
use App\Entities\Report;
use Carbon\Carbon;
use Faker\Provider\Uuid;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class ReportRepo
{
	protected $report;
	protected $file;

	public function __construct()
	{
		$this->report = new Report;
		$this->file = new File;
	}

	public function fetch()
	{
		//
	}

	public function store($data)
	{
		$this->report->create([
			'user_id' => $data['user_id'],
			'name' => $data['name'],
			'query' => $data['query'],
			'type' => $data['interval'],
			'when' => $data['crontab'],
			'description' => $data['description'],
			'active' => 0,
		]);

		return $this->report;
	}

	public function fetchPending($crontab)
	{
		//$crontab = '00 12 * * 2';

		$conditions = [
			'when'		=>	$crontab
		];

		$this->report = $this->report->where($conditions)->get();

		if (count($this->report) > 0) {
			$this->_deactivate();

			return $this->report;
		}

		return false;
	}

	public function fetchPendingThreads()
	{
		$limit = Carbon::now()->subMonths(6)->toDateTimeString();
		$query = "SELECT id_customer_thread, id_order, email, supplier_name FROM ps_customer_thread WHERE status = 'open' AND date_add >= '$limit' ORDER BY id_customer_thread DESC;";
		// $query = "SELECT count(*) FROM ps_customer_thread WHERE status = 'open' ORDER BY id_customer_thread DESC;";
		// dd($query);

		$result = $this->_runQuery($query);
		// $content = $this->_reconstructQuery($result);
		// dd($result);
		return $result;
	}

	public function fetchFile($report_name)
	{
		// dd($this->file->where('name', $report_name)->first());
		// return $this->file->where('name', $report_name)->first();
	}

	public function saveThread($thread)
	{
		// dd($this->file->where('name', $report_name)->first());
		// return $this->file->where('name', $report_name)->first();
	}

	public function createAndSave($report)
	{
		$query_result = $this->_runQuery($report->query);

		if (!$query_result) {
			Log::error('ReportRepo - createAndSave - Failed running query for '. $report->name);

			return false;
		}

		$content = $this->_reconstructQuery($query_result);

		$name = Uuid::uuid();

		$this->file = $this->file->create([
			'name' => $name,
			'state' => 1,
			'report_id' => $report->id
		]);

		$this->_alocate($content);
	}

	public function fetchForUser($user_id)
	{
		return $this->report->where('user_id', $user_id)->orderBy('created_at', 'desc')->get();
	}

	public function newSale($content, $user_id, $partner)
	{
		$name = Uuid::uuid();

		$this->file = $this->file->create([
			'name' => $name,
			'state' => 1,
			'report_id' => 0,
			'blame' => $user_id,
			'partner' => $partner
		]);

		$this->_alocate($content);
	}

	public function getSales()
	{
		$user_id = Auth::user()->id;

		return $this->file->where(['report_id' => 0, 'blame' => $user_id])->orderBy('id', 'desc')->get();
	}

	private function _alocate($content)
	{
		if (Storage::disk('report')->put($this->file->name.'.csv', $content)) {
			$this->file->state = 0;

		} else {
			$this->file->state = 2;
		}
	}

	private function _alocateCsv($content)
	{
		//dd($content);
	}

	private function _runQuery($query)
	{
		try {
			$result = DB::connection('mysql')->select($query);
			Log::info('Attempting to run query: ' . json_encode($result));

			return $result;

		} catch (\Exception $e){
			Log::info('ReportRepo - process - Failed running Query - '. json_decode($e));
		}
	}

	private function _reconstructQuery($query)
	{
		$columns = '';

		// Get Columns
		foreach ($query[0] as $key => $value) {
			$columns .= $key . ';';
		}
		$columns .= "\n";

		// Get Values
		foreach ($query as $q) {
			foreach ($q as $value) {
				$columns .= $value . ';';
			}
			$columns .= "\n";
		}

		// Remove leftover ','s
		return $columns;
	}

	private function _deactivate()
	{
		foreach ($this->report as $report) {
			Report::where('id', $report->id)->update(['active' => 1]);
		}
	}
}
