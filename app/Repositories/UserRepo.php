<?php

namespace App\Repositories;

// use App\Entities\Report;
use App\Entities\User;

class UserRepo
{
	protected $user;

	public function __construct()
	{
		$this->user = new User;
	}

	public function fetch()
	{
		$users = $this->user->all();

		return $users;
	}

	public function store($data)
	{
		//
	}
}
