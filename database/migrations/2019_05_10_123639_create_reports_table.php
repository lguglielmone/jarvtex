<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTable extends Migration
{
	/**
	* Run the migrations.
	*
	* @return void
	*/
	public function up()
	{
		Schema::create('reports', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->integer('user_id')
				->references('id')->on('users')
				->onDelete('cascade');
			$table->string('name')->nullable();
			$table->string('query')->nullable();
			$table->string('type')->nullable();
			$table->string('when')->nullable();
			$table->longText('description')->nullable();
			$table->integer('active')->nullable();

			$table->timestamps();
		});
	}

	/**
	* Reverse the migrations.
	*
	* @return void
	*/
	public function down()
	{
		Schema::dropIfExists('reports');
	}
}
