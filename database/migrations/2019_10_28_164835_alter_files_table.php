<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFilesTable extends Migration
{
	/**
	* Run the migrations.
	*
	* @return void
	*/
	public function up()
	{
		Schema::table('files', function (Blueprint $table) {

			$table->integer('blame')->after('state')->nullable();
			$table->string('partner')->after('blame')->nullable();

		});
	}

	/**
	* Reverse the migrations.
	*
	* @return void
	*/
	public function down()
	{
		Schema::table('files', function (Blueprint $table) {
			$table->dropColumn('blame');
		});

		Schema::table('files', function (Blueprint $table) {
			$table->dropColumn('partner');
		});
	}
}
