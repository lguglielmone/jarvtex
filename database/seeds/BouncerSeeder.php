<?php

use Illuminate\Database\Seeder;
use App\Entities\User;

class BouncerSeeder extends Seeder
{
	/**
	* Run the database seeds.
	*
	* @return void
	*/
	public function run()
	{
		//? Roles
		$admin = Bouncer::role()->firstOrCreate([
			'name' => 'admin',
			'title' => 'Administrador',
		]);

		$plebe = Bouncer::role()->firstOrCreate([
			'name' => 'plebe',
			'title' => 'Plebeyos',
		]);


		//? Abilities
		$godMode = Bouncer::ability()->firstOrCreate([
				'name' => 'god-mode',
				'title' => 'God Mode',
		]);

		$readMenu = Bouncer::ability()->firstOrCreate([
				'name' => 'read-menu',
				'title' => 'Menu Read',
		]);
		$read = Bouncer::ability()->firstOrCreate([
				'name' => 'read',
				'title' => 'Read',
		]);


		// Declarations
		Bouncer::allow($admin)->everything();
		Bouncer::allow($plebe)->to($read);
		
		$user = User::where('id', 1)->first();
		Bouncer::assign('admin')->to($user);
	}
}
