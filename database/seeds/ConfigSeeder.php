<?php

use Illuminate\Database\Seeder;

class ConfigSeeder extends Seeder
{
	/**
	* Run the database seeds.
	*
	* @return void
	*/
	public function run()
	{
		DB::table('configurations')->insert([
			'name' => 'partner',
			'value' => 'tije-travel',
			'slug' => 'Tije'
		]);

		DB::table('configurations')->insert([
			'name' => 'partner',
			'value' => 'despegar',
			'slug' => 'Despegar'
		]);

		DB::table('configurations')->insert([
			'name' => 'partner',
			'value' => 'garbarino-viajes',
			'slug' => 'Garbarino'
		]);

		DB::table('configurations')->insert([
			'name' => 'endpoint',
			'value' => 'https://partner-club-dev.aper.net',
			'slug' => 'Servicio Intermedio'
		]);

		DB::table('configurations')->insert([
			'name' => 'endpoint',
			'value' => 'https://riskevaluator-dev.aper.net',
			'slug' => 'API Risk'
		]);
	}
}
