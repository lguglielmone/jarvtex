
<div class="sidebar" data-color="white">

	<div class="logo">
		<a href="{{ route('index') }}" class="simple-text logo-item">
			<img src="https://www.aper.com/wp-content/uploads/2018/09/aper-logo-transparent.png" alt="Aper">
		</a>
	</div>

	<div class="sidebar-wrapper" id="sidebar-wrapper">
		<ul class="nav">
			@guest
				<li class="active">
					<a href="{{ route('login') }}"> <i class="fas fa-sign-in-alt"></i>
						<p class="menu-text">Iniciar Sesion</p>
					</a>
				</li>
			@else
			@can('read-menu', \app\Entities\User::class)


			<li>
				<a href="{{ route('report-new') }}"> <i class="fas fa-folder-plus"></i>
					<p class="menu-text">Nuevo Reporte</p>
				</a>
			</li>

			<li class="{{--active--}}">
				<a href="{{ route('user-reports') }}"> <i class="fas fa-file-csv"></i>
					<p>Mis Reportes</p>
				</a>
			</li>

			<li class="disabled">
				<a href="{{ route('risk-data') }}"> <i class="fas fa-chart-pie"></i>
					<p>Riesgo</p>
				</a>
			</li>

			<li>
				<a href="{{ route('users') }}">

					<i class="now-ui-icons users_single-02"></i>

					<p>Usuarios</p>
				</a>
			</li>

			{{-- <li class="">
				<a href="{{ route('role-abilities') }}"> <i class="fas fa-users-cog"></i>
					<p>Permisos</p>
				</a>
			</li> --}}

			<li class="{{--active--}}">
				<a href="{{ route('sale-consult') }}"> <i class="fas fa-plane-departure"></i>
					<p>Turismo</p>
				</a>
			</li>

			<li class="{{--active--}}">
				<a href="{{ route('config') }}"> <i class="fas fa-cog"></i>
					<p>Configuracion</p>
				</a>
			</li>
			@endcan

			<li class="sign-out">
				<a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> <i class="fas fa-sign-out-alt"></i>
					<p>Cerrar Sesion</p>
				</a>
			</li>
			<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
				@csrf
			</form>
			@endguest
		</ul>
	</div>
</div>
