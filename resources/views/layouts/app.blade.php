<!DOCTYPE html>

<html>

<head>
	@include('inc.head')
	@yield('styles')
</head>

<body>
	{{-- @guest
		@include('index')
	@else --}}
	@include('layouts.navbar')

	@auth
		<div class="index">
			@include('layouts.menu')

			<div class="content">
				<div class="row">
					<div class="col-md-12">
						@yield('content')

					</div>
				</div>

			</div>

		</div>
	@else

	<div class="landing">
		@include('landing')
	</div>

	@endauth

	{{-- </div> --}}
	{{-- @include('user.login') --}}
</body>
@yield('scripts')
</html>
