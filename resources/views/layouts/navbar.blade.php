
<nav class="navbar navbar-expand-md bg-dark navbar-dark">
	<div class="container">
		<a class="navbar-brand nav-link" href="{{ url('/') }}">
			{{ config('app.name', 'Business Reports') }}
		</a>
		{{-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
			<span class="navbar-toggler-icon"></span>
		</button> --}}

		<div class="collapse navbar-collapse" id="navbarSupportedContent">

			<ul class="navbar-nav ml-auto">

				@guest
				<li class="nav-item">
					<a class="nav-link" href="https://authentication.aper.store/home">{{ __('Registrarme') }}</a>
				</li>

				<li class="nav-item">
					<a class="nav-link" id="login" href="{{ route('login') }}">{{ __('Login') }}</a>
				</li>

				@else
				<li class="nav-item">
					<button class="btn-user" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }}</button>

					<div class="dropdown-menu">
						<a class="dropdown-item" href="#">Action</a>
						<a class="dropdown-item" href="#">Another action</a>
						<a class="dropdown-item" href="#">Something else here</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="#">Cerrar Sesion</a>
					</div>
				</li>

				@endguest
			</ul>
		</div>
	</div>
</nav>
