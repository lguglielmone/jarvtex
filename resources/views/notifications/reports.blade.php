
@isset($notification)

  @if($notification = 'success')
  <div class="row justify-content-center">
    <div class="col-md-12">
      <div class="alert alert-success">
        <strong>Exito!</strong> Cuando su reporte este listo, se le notificara por mail.
      </div>
    </div>
	</div>

	@elseif($notification = 'error')
	<div class="row justify-content-center">
    <div class="col-md-12">
      <div class="alert alert-danger">
        <strong>Error!</strong> Revise la informacion ingresada.
      </div>
    </div>
	</div>

	@elseif($notification = 'fail')
	<div class="row justify-content-center">
    <div class="col-md-12">
      <div class="alert alert-danger">
        <strong>Error!</strong> No se pudo guardar el reporte.
      </div>
    </div>
	</div>

	@endif

@endisset
