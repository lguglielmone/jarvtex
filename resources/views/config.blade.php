@extends('layouts.app')


@section('content')

<div class="card">
		<div class="card-header row">
			<div class="col-md-12 center">
				<h4 class="card-title">Configuracion</h4>
			</div>
			{{-- <div class="col-md-2 newfile">
				<div>
					<a href="{{ route('fetch-sales') }}" class="btn btn-info">Generar nuevo reporte</a>
				</div>
			</div> --}}
		</div>

		<hr>

		<div class="card-body row">

			<div class="col-md-12">

				<div class="row">
					<div class="col-md-6">
						<h3>Turismo</h3> <hr>
						@if (isset($partners))
							@foreach ($partners as $partner)
							<form action="{{ route('save-config-partners') }}" method="post"> @csrf
							<input type="hidden" name="partner_id" value="{{ $partner->id }}">
								<div class="form-group row">
									<div class="col-md-4">
										<label for="partners">{{ $partner->slug }}</label>
									</div>
									<div class="col-md-4">
										<input type="text" value="{{ $partner->value }}" name="value"></input>
									</div>
									<div class="col-md-2">
										<button type="submit" class="btn btn-default">Guardar</button>
									</div>
									<div class="col-md-2"></div>
								</div>
							</form>
							<hr>
							@endforeach
						@else
						<span>Todavia no se han configurado proveedores de turismo</span>
						@endif
					</div>

					<div class="col-md-6">
						<h3>Endpoints</h3> <hr>
						@if (isset($endpoints))
							@foreach ($endpoints as $endpoint)
							<form action="{{ route('save-config-endpoints') }}" method="post"> @csrf
								<input type="hidden" name="endpoint_id" value="{{ $endpoint->id }}">
								<div class="form-group row">
									<div class="col-md-4">
										<label for="endpoints">{{ $endpoint->slug }}</label>
									</div>
									<div class="col-md-4">
										<input type="text" value="{{ $endpoint->value }}" name="endpoint"></input>
									</div>
									<div class="col-md-2">
										<button type="submit" class="btn btn-default">Guardar</button>
									</div>
									<div class="col-md-2"></div>
								</div>
							</form>
							<hr>
							@endforeach
						@else
						<span>Todavia no se han configurado endpoints</span>
						@endif
					</div>
				</div>

@endsection

@section('styles')

<style>

	hr {
		color: #2b2b2b ;
		height: 4px;
	}

</style>

@endsection
