@extends('layouts.app')

@section('content')

<div class="card text-left">

	<div class="card-header">
		<h4 class="card-title">Roles & Permisos</h4>
	</div>

	<div class="card-body">
		<h2>Roles</h2>
			<ul class="list-group">

				<form method="POST" action="{{ route('save-role-abilities') }}"> @csrf
				@foreach ($roles as $role)
					<input type="hidden" name="uid" value="{{$role->id}}">
					<li class="list-group-item">
						<div class="row">
							<div class="col-md-3">
								{{$role->name}}
							</div>
							<div class="col-md-3">
								{{$role->email}}
							</div>
							<div class="col-md-3">
								<select name="role" id="user-roles" class="form-control" {{--$user->isAn('admin') ? 'disabled' : ''--}}>
									<option value=""></option>
										@foreach ($roles as $role_value => $role_name)
										<option value="{{$role_value}}" {{--$user->isAn($role_name) ? 'selected' : ''--}} >{{$role_name}}</option>
										@endforeach
								</select>
							</div>
							<div class="col-md-3">
								<button type="submit" class="btn btn-dark pull-right bounce-submit" {{--$user->isAn('admin') ? 'disabled' : ''--}}>Guardar</button>
							</div>
						</div>
					</li>
				</form>
				@endforeach
				{{-- <li class="list-group-item active">Active item</li>
				<li class="list-group-item">Item</li>
				<li class="list-group-item disabled">Disabled item</li> --}}
			</ul>
			<button type="submit" class="btn btn-dark pull-right">Guardar</button>
	</div>
</div>

@endsection
