
{{-- <div class="h-100 landing overlay" style="background-image: url({{ asset('imgs/landing.jpg') }}); background-size: cover; height: 100%;">

	<div class="signature">
		<img src="https://www.aper.com/wp-content/uploads/2018/09/aper-logo-transparent.png" alt="Aper">
	</div>

	<header class="masthead">
		<div class="container h-100">
			<div class="row h-100 align-items-center justify-content-center text-center">
				<div class="col-lg-10 align-self-end">
					<h1 class="text-uppercase text-black font-weight-bold">Reportes Comerciales</h1> <br>
					<hr class="divider my-4">
				</div>
				<div class="col-lg-8 align-self-baseline">
					<p class="text-white-75 font-weight-light mb-5 nope">Start Bootstrap can help you build better websites using the Bootstrap framework! Just download a theme and start customizing, no strings attached!</p>
					<a class="btn btn-primary btn-lg js-scroll-trigger login-btn" href="{{ route('login') }}">Entrar</a>
				</div>
			</div>
		</div>
	</header>

</div> --}}

<div class="card card-image card-landing">
  <div class="text-white text-center rgba-stylish-strong py-5 px-4">
    <div class="py-5">

      <!-- Content -->
      <h5 class="h5 orange-text"> <i class="far fa-life-ring"></i> Jarvis</h5>
      <h2 class="card-title h2 my-4 py-2">Sistema de Reportes</h2>
      <p class="mb-4 pb-2 px-md-5 mx-md-5">Genera, programa y descarga tus reportes de manera segura, confidencial y sin afectar produccion.</p>
			{{-- <a class="btn peach-gradient" href="{{ route('login') }}"><i class="fas fa-sign-in-alt"></i> Entrar</a> --}}
			<button type="button" class="btn btn-outline-info btn-link"><a href="{{ route('login') }}" class="nav-a">Entrar</a></button>

    </div>
  </div>
</div>

<div class="signature">
	<img src="https://www.aper.com/wp-content/uploads/2018/09/aper-logo-transparent.png" alt="Aper">
</div>

<style>
.landing {
	background-image: url({{ asset('imgs/land.png') }}) ;
}
</style>


</html>
