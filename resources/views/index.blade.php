{{-- <div class="index-back">
	<div class="container">
		<div class="row row-eq-height">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<div class="card bg-dark text-white deck">
					<div class="card-body text-center">

						<h4 class="card-title">Business Reports</h4>

						<p class="card-text">
							<span class="badge badge-pill badge-danger ">Login</span>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div> --}}

<body class="hm-gradient">
			<div class="container mt-4">
					<div class="row mb-4">
							<div class="col-md-12">
							<div class="card" style="background-image: url({{ asset('imgs/index.png') }});">
											<div class="text-white text-center d-flex align-items-center py-5 px-4 my-5">
													<div>
															<h1 class="card-title pt-3 mb-5 font-bold">
																<strong>B</strong>usiness
																<strong>R</strong>eports
															</h1>
															<p class="mx-5 mb-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat fugiat,
																	laboriosam, voluptatem, optio vero odio nam sit officia accusamus
																	minus error nisi architecto nulla ipsum dignissimos. Odit sed qui,
																	dolorum!.</p>
															<a class="btn btn-outline-white" href="{{ route('login') }}"><i class="fa fa-clone left"></i> Login</a>
													</div>
											</div>
									</div>
							</div>
					</div>
			</div>
</body>

<style>
@import url(https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css);
@import url(https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.4.3/css/mdb.min.css);

.hm-gradient {
    background-image: linear-gradient(to top, #f3e7e9 0%, #e3eeff 99%, #e3eeff 100%);
}
.darken-grey-text {
    color: #2E2E2E;
}
</style>
