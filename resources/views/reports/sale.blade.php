@extends('layouts.app')


@section('content')

<div class="card">
	<div class="card-header row">
		<div class="col-md-12">
			<h2 class="card-title">Nuevo Reporte (Turismo)</h2>
		</div>
	</div>

	<hr>

	<div class="card-body">
		<form action="{{ route('fetch-sales') }}" method="post" class="row"> @csrf <input type="hidden" name="user" value="{{ Auth::user()->id }}">

			<div class="col-md-2 form-group input-piece">
				<label for="partner">Proveedor</label>
				<select name="partner" class="form-control" required>
					<option value="all|Todos">Todos</option>
					@foreach ($partners as $partner)
					<option value="{{ $partner['slug'] . '|' . $partner['name'] }}">{{ $partner['name'] }}</option>
					@endforeach
				</select>
			</div>

			<div class="col-md-4 form-group input-group date">
				<label for="date-from">Desde</label>
				<div class="date-row">
					<input type="number" max="31" min="1" name="date-from-day" placeholder="{{date('d')}}" class="date-input form-control-sm " required>
					<input type="number" max="12" min="1" name="date-from-month" placeholder="{{date('m')}}" class="date-input form-control-sm" required>
					<input type="number" max="2030" min="2017" name="date-from-year" placeholder="{{date('Y')}}" class="date-input form-control-sm" required>
				</div>
			</div>

			<div class="col-md-4 form-group input-group date">
				<label for="date-to">Hasta</label>
				<div class="date-row">
					<input type="number" max="31" min="1" name="date-to-day" placeholder="{{date('d')}}" class="date-input form-control-sm" required value="{{date('d')}}">
					<input type="number" max="12" min="1" name="date-to-month" placeholder="{{date('m')}}" class="date-input form-control-sm" required value="{{date('m')}}">
					<input type="number" max="2030" min="2017" name="date-to-year" placeholder="{{date('Y')}}" class="date-input form-control-sm" required value="{{date('Y')}}">
				</div>
			</div>

			<div class="col-md-2 input-piece">
				<button type="submit" class="btn btn-default float-left">Generar</button>
			</div>

		</form>
	</div>
</div>

@endsection


@section('styles')
<style>
	.btn {
		float: right;
		margin-right: 9em;
	}

	.date-row {

	}
	form {
		padding: 15px;
	}

	.date {
		display: inline-block;
	}

	.input-piece {
		margin-top: auto;
	}
	.date-input {
		text-align: center;
	}
</style>
@endsection
