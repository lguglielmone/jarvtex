@extends('layouts.app')


@section('content')

	@foreach ($reports as $report)
		<div class="card text-left">
			<img class="card-img-top" src="holder.js/100px180/" alt="">
			<div class="card-body">
				<h4 class="card-title">{{$report->name}}</h4>
				<hr>

				<span>Reglas Inclumplidas</span>
				<canvas id="chDonut{{$report->id}}"></canvas>

			</div>
		</div>

		<div class="nope">
			@foreach ($report->rules as $rule => $value)
				<div class="nope" id="{{ "$report->id-$rule" }}">{{$value}}</div>
			@endforeach
		</div>
	@endforeach


@endsection


@section('styles')

<style>

</style>

@endsection

@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>

<script>

/* chart.js chart examples */

// chart colors
var colors = ['#00a69a','#28a745','#333333','#c3e6cb','#dc3545','#6c757d'];

/* 3 donut charts */
var donutOptions = {
  cutoutPercentage: 85,
  legend: {position:'bottom', padding:5, labels: {pointStyle:'circle', usePointStyle:true}}
};

// donut 1
var rules = document.getElementById("1-amount");
var chDonutData1 = {
    // labels: ['Bootstrap', 'Popper', 'Other'],

    datasets: [
      {
        backgroundColor: colors.slice(0,3),
        borderWidth: 0,
        data: [74, 11, 40]
      }
    ]
};

var chDonut1 = document.getElementById("chDonut1");
if (chDonut1) {
  new Chart(chDonut1, {
      type: 'pie',
      data: chDonutData1,
      options: donutOptions
  });
}

</script>
@endsection
