@extends('layouts.app')


@section('content')

<div class="card">
	<div class="card-header">
		<h4 class="card-title">Turismo</h4>
		<div class="float-right">
			<a href="{{ route('new-sale-consult') }}">
				<i class="fas fa-plus-circle"></i>
			</a>
			<i class="fas fa-plus-circle"></i>
		</div>
	</div>

	<hr>

	<div class="card-body">

		<h4>Generar CSV</h4>

		<form method="POST" action="{{ route('get-sales') }}"> @csrf

				<div class="row">

					<div class="col-md-6">
						<label for="report-name">Nombre y Apellido</label>
						<input type="text" name="blame" id="blame" class="form-control" required>
					</div>
				</div>

				<hr>

				<div class="row" id="report_repeat">

					<div class="col-md-4">
						<label for="report-day">Partner</label>
						{{-- <input type="select" name="report-day" id="report-day" class="form-control"> --}}
						<input type="text" name="partner" id="partner" class="form-control" required>
					</div>

					<div class="col-md-2 form-group">
						<label for="report-hour">Hora</label>
						{{-- <input type="select" name="report-hour" id="report-hour" class="form-control"> --}}
						<input type="time" id="report-hour" name="report-hour" min="00:00" max="24:00" class="form-control report-time" required>
					</div>

					<div class="col-md-2">
						<label for="report-interval-repeat">Repetir</label>
						<input type="checkbox" name="report-interval-repeat" id="report-interval-repeat" class="form-control">
					</div>

					<div class="col-md-4"></div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<label for="report-query">Consulta</label>
						<textarea rows="5" name="report-query" id="report-query" class="form-control border rounded report-textarea" required></textarea>
					</div>
				</div>

				<button type="submit" class="btn btn-dark pull-right">Guardar</button>
			</form>

	</div>
</div>

@endsection


@section('styles')

<style>

</style>

@endsection
