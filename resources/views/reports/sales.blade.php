@extends('layouts.app')


@section('content')

<div class="card">
	<div class="card-header">
		<h4 class="card-title">Reportes - Turismo</h4>
		<a href="{{route('sale-form')}}" class="float-right btn btn-default new-report-btn">Nuevo reporte</a>
		{{-- <div class="col-md-6"> --}}


		{{-- </div> --}}
		{{-- <div class="col-md-2 newfile">
			<div>
				<a href="{{ route('fetch-sales') }}" class="btn btn-info">Generar nuevo reporte</a>
			</div>
		</div> --}}
	</div>


	<div class="card-body row">

		@if (!count($files))
		<div class="col-md-12">
			Todavia no se han generado reportes <i class="far fa-sad-tear"></i>
		</div>
		@else

		{{-- @foreach ($files as $file)

		<div class="col-md-2">
			<button type="button" class="btn btn-custom">
				<a href="{{ route('report-downloads'). '/'.$file['name'] }}" class="download-file-link">
					<div class="icon">
						<img src="{{asset('/img/icon-csv.png')}}" alt="CSV" height="50px" width="50px">
					</div>
					<div class="date">
						<p class="card-text download-file-link">{{ date('d-M-Y', strtotime($file['updated_at'])) }}</p>
					</div>
				</a>
			</button>
		</div>
		@endforeach --}}

		{{--                          New stuff                          --}}

		<table class="table">
			<tr class="">
				<th scope="col"> # </th>
				<th scope="col"> Proveedor </th>
				<th scope="col"> Fecha </th>
				<th scope="col">  </th>
			</tr>

			@foreach ($files as $key => $file)

			<tr>
				<td>
					{{$key + 1}}
				</td>
				<td scope="row">
					{{$file->partner}}
				</td>
				<td>
					<small>{{date('d-M-Y h:i:s', strtotime($file['updated_at']))}}</small>
				</td>
				<td>
						<button type="button" class="btn btn-custom">
							<a href="{{ route('report-downloads'). '/'.$file['name'] }}" class="download-file-link"> <i class="fas fa-arrow-circle-down"></i></a>
						</button>
				</td>
			</tr>

			@endforeach
		</table>

		@endif
	</div>
</div>

@endsection


@section('styles')

<style>
	.boxes {
		display: -webkit-inline-box;
	}
	.icon {
		display: -moz-box;
	}
	.box {
		text-align: center;
		margin: 5px 15px;
	}
	.box:hover {
		cursor: grab;
	}
	.file-error {
		border-color: firebrick;
	}
	.report-btn {
		color: red;
	}
	.newfile {
		padding: inherit;
	}
	hr {
		color: #2b2b2b ;
		height: 4px;
	}
	.btn-info {
		background-color: #2b2b2b !important;
		border-color: #2b2b2b !important;
		color: springgreen !important;
	}
	.btn-custom {
		background-color: transparent !important;
		border-color: darkslategray !important;
		color: darkslategray;
	}
	.btn-custom:hover {
		background-color: #2b2b2b !important;
		border-color: #00a69a !important;
		color: springgreen !important;
	}
	.download-file-link {
		color: darkslategray;
	}
	.download-file-link:hover {
		color: springgreen !important;
	}
	.btn-custom:hover a {
		color: springgreen !important;
	}
	a:hover, a:visited, a:link, a:active
	{
		text-decoration: none;
	}
	.new-report-btn {
		position: absolute;
		margin-left: 85%;
		margin-top: -3em;
	}
</style>

@endsection
