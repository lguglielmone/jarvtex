@extends('layouts.app')

@section('content')

@if (is_null($reports))
		No hay reportes...
@else

<h1>Reportes</h1>
<hr>
<div class="reports">

	<div class="row">
		<div class="col-md-4 reports-head">
			Nombre
		</div>
		<div class="col-md-4 reports-head">
			Usuario
		</div>
		<div class="col-md-4 reports-head">
			Fecha
		</div>
	</div>

	@foreach ($reports as $report)
		<div class="row">
			<div class="col-md-4 report-content" id="reports-name">
				{{ $report['name'] }}
			</div>
			<div class="col-md-4 report-content" id="reports-user">
				{{ $report->user->name }}
			</div>
			<div class="col-md-4 report-content" id="reports-date">
				{{ $report['created_at'] }}
			</div>
		</div>
	@endforeach


</div>
@endif

{{-- <table class="table">
	<thead>
		<th scope="col">Nombre</th>
		<th scope="col">Usuario</th>
		<th scope="col">Fecha</th>
	</thead>
	<tbody>
		@foreach ($reports as $report)

		<td scope="col">
			<div name="report-name" id="report-name" class="report-input">$report->name)</div>
		</td>

		<td scope="col">
			<div name="report-blame" id="report-blame" class="report-input">$report->user_id)</div>
		</td>

		<td scope="col">
			<div name="report-when" id="report-when" class="report-input">$report->created_at)</div>
		</td>

		@endforeach
	</tbody>
	<tr>
	</table>




	<form action="{{ route('report-store') }}">
		<input type="hidden" value="1" name="report-user" id="user_id">
		<table class="table">
			<thead>
				<th scope="col">Nombre</th>
				<th scope="col"></th>
				<th scope="col">Cuando</th>
				<th scope="col"></th>
			</thead>
			<tbody>

				<td scope="col">
					<input type="text" name="report-query" id="report-query" class="form-control report-input" placeholder="SELECT * FROM <table_name> ;" aria-describedby="helpId">
					</td>

					<td scope="col">
						<input type="text" name="report-type" id="report-type" class="form-control report-input" placeholder="" aria-describedby="helpId">
					</td>

					<td scope="col">
						<input type="text" name="report-when" id="report-when" class="form-control report-input" placeholder="" aria-describedby="helpId">
					</td>

					<td scope="col">
						<button type="submit" class="btn btn-dark">Submit</button>
					</td>

				</tbody>
				<tr>
				</table>
			</form> --}}


@endsection
