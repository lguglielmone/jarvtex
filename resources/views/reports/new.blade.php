@extends('layouts.app')

@section('content')

@include('notifications.reports')

<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header"><strong> Nuevo Reporte </strong></div> {{-- EDIT --}}
        <div class="card-body">
					<form method="POST" action="{{ route('report-store') }}"> @csrf
						<input type="hidden" value="{{ $id }}" name="report-user" id="user_id">

						<div class="row">

							<div class="col-md-4">
								<label for="report-name">Nombre</label>
								<input type="text" name="report-name" id="report-name" class="form-control" required>
							</div>
							{{-- <div class="col-md-8"></div> --}}
							<div class="col-md-12">
								<label for="report-desc">Descripcion</label>
								<textarea rows="5" name="report-desc" id="report-desc" class="form-control border rounded report-textarea" required></textarea>
								{{-- <input type="text" name="report-desc" id="report-desc" class="form-control"> --}}
							</div>
						</div>

						<hr>

						<div class="row" id="report_repeat">

							<div class="col-md-4">
								<label for="report-day">Dia</label>
								{{-- <input type="select" name="report-day" id="report-day" class="form-control"> --}}
								<select class="form-control" name="report-day" required>
										<option value=""></option>
										<option value="1">Lunes</option>
										<option value="2">Martes</option>
										<option value="3">Miercoles</option>
										<option value="4">Jueves</option>
										<option value="5">Viernes</option>
										<option value="6">Sabado</option>
										<option value="0">Domingo</option>
									</select>
							</div>

							<div class="col-md-2 form-group">
								<label for="report-hour">Hora</label>
								{{-- <input type="select" name="report-hour" id="report-hour" class="form-control"> --}}
								<input type="time" id="report-hour" name="report-hour" min="00:00" max="24:00" class="form-control report-time" required>
							</div>

							<div class="col-md-2">
								<label for="report-interval-repeat">Repetir</label>
								<input type="checkbox" name="report-interval-repeat" id="report-interval-repeat" class="form-control">
							</div>

							<div class="col-md-4"></div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<label for="report-query">Consulta</label>
								<textarea rows="5" name="report-query" id="report-query" class="form-control border rounded report-textarea" required></textarea>
							</div>
						</div>

						<button type="submit" class="btn btn-dark pull-right">Guardar</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>


<script>

	$(document).on('click', '#report-interval-repeat', function (event) {
		console.log('Repetir');
		alternate('report_no_repeat', 'report_repeat');
	});

	$(document).on('click', '#report-interval-no-repeat', function (event) {
		console.log('No Repetir');
		alternate('report_repeat', 'report_no_repeat');
	});

	function alternate (show, hide) {
		console.log('#'+hide);
		// $('#'+show).css('display') = 'block';
		$('#'+show).style.display = 'block';
		$('#'+hide).css('display') = 'none';
	}
</script>



{{-- <div class="deck">
	<h1>Nuevo Reporte</h1>
	<form action="{{ route('report-store') }}">
	<input type="hidden" value="{{ $id }}" name="report-user" id="user_id">
		<table class="table">
			<thead>
				<th scope="col">Nombre</th>
				<th scope="col">Consulta</th>
				<th scope="col">Tipo</th>
				<th scope="col">Cuando</th>
				<th scope="col"></th>
			</thead>
			<tbody>

				<td scope="col">
					<input type="text" name="report-name" id="report-name" class="form-control report-input" aria-describedby="helpId">
				</td>

				<td scope="col">
					<input type="text" name="report-query" id="report-query" class="form-control report-input" placeholder="SELECT * FROM <table_name> ;" aria-describedby="helpId">
				</td>

				<td scope="col">
					<input type="text" name="report-type" id="report-type" class="form-control report-input" placeholder="" aria-describedby="helpId">
				</td>

				<td scope="col">
					<input type="text" name="report-when" id="report-when" class="form-control report-input" placeholder="" aria-describedby="helpId">
				</td>

				<td scope="col">
					<button type="submit" class="btn btn-dark">Submit</button>
				</td>

			</tbody>
			<tr>
		</table>
	</form>
</div> --}}

@endsection
