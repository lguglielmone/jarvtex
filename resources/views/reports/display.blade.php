@extends('layouts.app')


@section('content')

<div class="card">
	<div class="card-header">
		<h4 class="card-title">Mis Reportes</h4>

		@if (count($reports))
		<div class="row">

			<div class="col-md-2 offset-3">
				<p class="card-text">
					Pendientes: <strong style="color:crimson">{{$pending}}</strong>
				</p>
			</div>

			<div class="col-md-2">
				<p class="card-text">
					En Cola: <strong style="color:">0</strong>
				</p>
			</div>

			<div class="col-md-2">
				<p class="card-text">
					Totales: <strong style="color:green">{{ count($reports) }}</strong>
				</p>
			</div>

		</div>
		@endif
	</div>

	<hr>

	<div class="card-body">

		@if (!count($reports))
		No tenes reportes <i class="far fa-sad-tear"></i>
		@else
		<div id="accordianId" role="tablist" aria-multiselectable="true">
			<ul class="list-group list-group-flush">

				@foreach ($reports as $report)
				<div class="card">
					<div class="card-header rep-btn-container row {{ isset($report->file[0]) ? 'rep-active-pill' : 'rep-disabled-pill'}}" aria-disabled="true" id="report-{{$report->id}}-head" data-toggle="collapse" data-target="#report-{{$report->id}}-body" aria-expanded="false" aria-controls="collapseOne">
						<div class="col-md-6">
							{{ $report->name }}

						@if (isset($report->file[0]))
						<span class="badge badge-pill badge-danger">
							{{ count($report->file) }}
						</span>
						@endif
					</div>
						{{-- <span class="report-query">
							{{ $report->query }}
						</span> --}}
					<div class="col-md-6">
						{{-- <a class="report-edit" href="{{route('report')."/edit/$report->id"}}">
							<i class="far fa-edit jarvicon"></i>
						</a> --}}
						<span class="pull-right">
							<i class="fas fa-chevron-down"></i>
						</span>
					</div>
					</div>

					<div id="report-{{$report->id}}-body" class="collapse" aria-labelledby="report-{{$report->id}}-head" data-parent="#accordion">
						<div class="card-body row">

							@if (isset($report->file[0]))

							@foreach ($report->file as $file)
							<div class="col-md-3 offset-3 file-container">
								{{$file->created_at}}
								<a class="report-download" href="{{ route('report')."/download/$file->name" }}">
									<i class="fas fa-download jarvicon"></i>
								</a>
							</div>
							@endforeach

							@else
							<div class="col-md-6 offset-1 file-container">
								<span>
									<strong>Todavia no se ha ejecutado su reporte.</strong>
								</span>
							</div>
							@endif
							<div class="col-md-12">
								<a class="report-delete" href="{{route('report')."/delete/$report->id"}}">
									<i class="fas fa-trash-alt jarvicon"></i>
								</a>
							</div>

						</div>
					</div>
				</div>
				@endforeach
			</ul>
		</div>
		@endif
	</div>
</div>

@endsection


@section('styles')

<style>
	.file-error {
		border-color: firebrick;
	}

	.report-btn {
		color: red;
	}
	.jarvicon {

	}
</style>

@endsection
